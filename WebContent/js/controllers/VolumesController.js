'use strict';

angular.module('web')
    .controller('VolumesController', ['$scope', '$rootScope', '$state', '$q', 'Retention', '$filter', 'Storage',
                'Regions', 'ITEMS_BY_PAGE', 'DISPLAY_PAGES', '$modal', 'Volumes', 'Tasks', 'Zones', 'Instances',
                'Backups',
        function ($scope, $rootScope, $state, $q, Retention, $filter, Storage, Regions, ITEMS_BY_PAGE, DISPLAY_PAGES,
                  $modal, Volumes, Tasks, Zones, Instances, Backups) {

        $scope.maxVolumeDisplay = 5;
        $scope.itemsByPage = ITEMS_BY_PAGE;
        $scope.displayedPages = DISPLAY_PAGES;

        $scope.stateColorClass = {
            "in-use": "success",
            "creating": "error",
            "available": "info",
            "deleting": "error",
            "deleted": "error",
            "error": "error",
            "removed": "danger"
        };

        $scope.textClass = {
            'false': 'Select',
            'true': 'Unselect'
        };

        $scope.iconClass = {
            'false': 'unchecked',
            'true': 'check'
        };

        var actions = {
            backup: {
                type: 'backup',
                bgClass: 'primary',
                modalTitle: 'Backup Volume',
                iconClass: 'cloud-download',
                description: 'start a backup task',
                buttonText: 'Add backup task'
            },
            restore: {
                type: 'restore',
                bgClass: 'success',
                modalTitle: 'Restore Backup',
                iconClass: 'cloud-upload',
                description: 'start a restore task',
                buttonText: 'Add restore task'

            },
            schedule: {
                type: 'schedule',
                bgClass: 'warning',
                modalTitle: 'Add Schedule',
                iconClass: 'time',
                description: 'add schedule',
                buttonText: 'Add schedule'
            }
        };

        $scope.isAllSelected = false;
        $scope.selectedAmount = 0;

        $scope.checkAllSelection = function () {
            var disabledAmount = $scope.volumes.filter(function (v) { return $scope.isDisabled(v)}).length;
            $scope.selectedAmount = $scope.volumes.filter(function (v) { return v.isSelected}).length;
            $scope.isAllSelected = ($scope.selectedAmount + disabledAmount == $scope.volumes.length);
        };

        $scope.selectAll = function () {
            $scope.volumes.forEach(function (volume) {
                doSelection(volume, !$scope.isAllSelected);
            });
            $scope.checkAllSelection();
        };

        $scope.toggleSelection = function (volume) {
            doSelection(volume, !volume.isSelected);
            $scope.checkAllSelection();
        };

        $scope.isConsistant = function(volume) {
            return volume.consistentBackup = !volume.consistentBackup;
        };

        var doSelection = function (volume, value) {
            if(volume.hasOwnProperty('isSelected')) {
                volume.isSelected = value;
            }
        };

        $scope.isDisabled = function (volume) {
            return volume.state === 'removed'
        };

        // ---------filtering------------

        $scope.showFilter = function () {
            var filterInstance = $modal.open({
                animation: true,
                templateUrl: './partials/modal.volume-filter.html',
                controller: 'modalVolumeFilterCtrl',
                resolve: {
                    tags: function () {
                        return $scope.tags;
                    },
                    instances: function () {
                        return $scope.instances;
                    }
                }
            });

            filterInstance.result.then(function (filter) {
                $scope.stAdvancedFilter = filter;
            });
        };

        var processVolumes = function (data) {
            $scope.tags = {};
            $scope.instances = [""];
            for (var i = 0; i < data.length; i++){
                for (var j = 0; j < data[i].tags.length; j++){
                    var tag = data[i].tags[j];
                    if (!$scope.tags.hasOwnProperty(tag.key)){
                        $scope.tags[tag.key] = [tag.value];
                    } else {
                        if ($scope.tags[tag.key].indexOf(tag.value) == -1){
                            $scope.tags[tag.key].push(tag.value);
                        }
                    }
                }

                var instance = data[i].instanceID;
                if (instance && $scope.instances.indexOf(instance) == -1){
                    $scope.instances.push(instance);
                }
                if (data[i].state !== 'removed') data[i].isSelected = false;
            }
            $scope.isAllSelected = false;
            return data;
        };

        //----------filtering-end-----------

        //-----------Volumes-get/refresh-------------

        $scope.changeRegion = function (region) {
            $scope.selectedRegion = region;
        };

        $scope.refresh = function () {
            $rootScope.isLoading = true;
            $scope.volumes = [];
            Volumes.get().then(function (data) {
                // hack for handling 302 status
                if (typeof data === 'string' && data.indexOf('<html lang="en" ng-app="web"')>-1) {
                    $state.go('loader');
                }
                $scope.volumes = processVolumes(data);
                $rootScope.isLoading = false;
            }, function () {
                $rootScope.isLoading = false;
            });
        };

        $scope.refresh();
        //-----------Volumes-get/refresh-end------------

        //-----------Volume-backup/restore/retention-------------

        $scope.restoreActions = ["Restore in AZ", "Attach to instance"];
        $scope.restoreAction = $scope.restoreActions[0];

        $scope.volumeAction = function (actionType) {
            $rootScope.isLoading = true;
            $scope.selectedVolumes = $scope.volumes.filter(function (v) { return v.isSelected; });
            $scope.actionType = actionType;
            $scope.action = actions[actionType];
            $scope.schedule = { name: '', cron: '', enabled: true };
            var selectedVolumesIds = $scope.selectedVolumes.map(function(volume) {
                return volume.volumeId;
            });

            Backups.send(selectedVolumesIds).then(function (result) {
                $scope.state = "done";
                $rootScope.isLoading = false;
                $scope.consistentBackups = result;
                $scope.selectedVolumes.forEach(function(volume) {
                    volume.consistentBackup = result[volume.volumeId];
                    if (!volume.consistentBackup) {
                        volume.isDisabled = true;
                    }
                    else {
                        volume.isDisabled = '';
                    }
                    return volume;
                });
            }, function () {
                $scope.state = "failed";
                $rootScope.isLoading = false;
            });

            var confirmInstance = $modal.open({
                animation: true,
                templateUrl: './partials/modal.volumeAction.html',
                scope: $scope
            });

            $q.all([Zones.get(), Zones.getCurrent(), Instances.get()])
                .then(function (results) {
                    $scope.zones = results[0];
                    $scope.selectedZone = results[1]["zone-name"] || "";
                    $scope.instances = results[2];
                    $scope.instance = $scope.instances[0];
                 })
                .finally(function () {
                    $rootScope.isLoading = false;
                });

            confirmInstance.result.then(function () {
                $rootScope.isLoading = true;

                var volList = $scope.selectedVolumes.map(function (volume) {
                    var result = {
                        "volumeId": volume.volumeId
                    };
                    switch (actionType) {
                        case 'restore':
                            // restoreAction == "Attach to instance"
                            if (volume.restoreAction === $scope.restoreActions[1]) {
                                result.instanceId = volume.instanceId || $scope.instance;
                            }
                            result.zone = volume.zone || $scope.selectedZone;
                        case 'backup':
                            result.consistentBackup = volume.consistentBackup;
                            break;
                        case 'schedule':
                            result.consistentBackup = volume.consistentBackup;
                            break;
                    }
                    return result;
                });


                var getNewTask = function(){
                    var newTask = {
                        type: actionType,
                        volumes: volList
                    };

                    switch (actionType) {
                        case 'schedule':
                            newTask.regular = true;
                            newTask.schedulerManual = false;
                            newTask.schedulerName = $scope.schedule.name;
                            newTask.cron = $scope.schedule.cron;
                            newTask.enabled = $scope.schedule.enabled;
                            break;
                    }

                    return newTask;
                };

                var t = getNewTask();
                Tasks.insert(t).then(function () {
                    $rootScope.isLoading = false;
                    if (actionType != 'schedule') {
                        var successInstance = $modal.open({
                            animation: true,
                            templateUrl: './partials/modal.task-created.html',
                            scope: $scope
                        });

                        successInstance.result.then(function () {
                            $state.go('app.tasks');
                        });
                    }
                }, function (e) {
                    $rootScope.isLoading = false;
                    console.log(e);
                });

            });

        };

        var getShowRule = function (rule) {
            var showRules = {};
            angular.forEach($scope.rule, function (value, key) {
                showRules[key] = value > 0;
            });
            Object.defineProperty(showRules, 'never', {
                get: function() {
                    return !$scope.showRetentionRule.size && !$scope.showRetentionRule.count && !$scope.showRetentionRule.days;
                },
                set: function(value) {
                    if (value){
                        $scope.showRetentionRule.size = false;
                        $scope.showRetentionRule.count = false;
                        $scope.showRetentionRule.days = false;
                    }
                }
            });
            return showRules;
        };
        $scope.retentionRule = function (volume) {
            $rootScope.isLoading = true;
            Retention.get(volume.volumeId).then(function (data) {

                $scope.rule = {
                    size: data.size,
                    count: data.count,
                    days: data.days
                };
                $scope.showRetentionRule = getShowRule($scope.rule);

                $rootScope.isLoading = false;

                var retentionModalInstance = $modal.open({
                    animation: true,
                    templateUrl: './partials/modal.retention-edit.html',
                    scope: $scope
                });

                retentionModalInstance.result.then(function () {
                    $rootScope.isLoading = true;
                    var rule = angular.copy($scope.rule);
                    angular.forEach(rule, function (value, key) {
                        rule[key] = $scope.showRetentionRule[key] ? rule[key] : 0
                    });
                    rule.volumeId = data.volumeId;

                    Retention.update(rule).then(function () {
                        $rootScope.isLoading = false;
                    }, function () {
                        $rootScope.isLoading = false;
                    })
                });

            }, function () {
                $rootScope.isLoading = false;
            });

        }
    }]);