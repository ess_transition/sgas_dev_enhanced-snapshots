'use strict';

angular.module('web')
    .controller('modalSystemBackupCtrl', ['$scope', '$rootScope', '$modalInstance', 'System', function ($scope, $rootScope, $modalInstance, System) {
        $scope.state = 'ask';

        $scope.sendTask = function () {
            $rootScope.isLoading = true;
            System.backup().then(function () {
                $scope.state = "done";
                $rootScope.isLoading = false;
            }, function () {
                $scope.state = "failed";
                $rootScope.isLoading = false;
            });

        }
    }]);