package com.sungardas.enhancedsnapshots.rest;

import com.sungardas.enhancedsnapshots.aws.dynamodb.model.TaskEntry;
import com.sungardas.enhancedsnapshots.dto.MessageDto;
import com.sungardas.enhancedsnapshots.dto.TaskDto;
import com.sungardas.enhancedsnapshots.exception.EnhancedSnapshotsException;
import com.sungardas.enhancedsnapshots.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;
import java.text.ParseException;

import static org.springframework.http.HttpStatus.OK;


@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getTasks() throws ParseException {
        try {
            return new ResponseEntity(taskService.getAllTasks(), OK);
        } catch (EnhancedSnapshotsException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NO_CONTENT);
        }
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.GET, value = "/{volumeId}")
    public ResponseEntity getTasks(@PathVariable String volumeId) throws ParseException {
        try {
            return new ResponseEntity(taskService.getAllTasks(volumeId), OK);
        } catch (EnhancedSnapshotsException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NO_CONTENT);
        }
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.GET, value = "/regular/{volumeId}")
    public ResponseEntity getRegularTasks(@PathVariable String volumeId) throws ParseException {
        try {
            return new ResponseEntity(taskService.getAllRegularTasks(volumeId), OK);
        } catch (EnhancedSnapshotsException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NO_CONTENT);
        }
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.GET, value = "/regular")
    public ResponseEntity getRegularTasks() throws ParseException {
        try {
            return new ResponseEntity(taskService.getAllRegularTasks(), OK);
        } catch (EnhancedSnapshotsException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NO_CONTENT);
        }
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.POST, value = "/backup")
    public ResponseEntity<MessageDto> addBackupTask(@RequestBody TaskDto taskInfo, Principal principal) {
        taskInfo.setSchedulerName(principal.getName());
        taskInfo.setRegular(Boolean.FALSE.toString());
        taskInfo.setType(TaskEntry.TaskEntryType.BACKUP.getType());
        return new ResponseEntity(new MessageDto(taskService.createTask(taskInfo)), OK);
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.POST, value = "/backup/regular")
    public ResponseEntity<MessageDto> addRegularBackupTask(@RequestBody TaskDto taskInfo) {
        taskInfo.setRegular(Boolean.TRUE.toString());
        taskInfo.setType(TaskEntry.TaskEntryType.BACKUP.getType());
        return new ResponseEntity(new MessageDto(taskService.createTask(taskInfo)), OK);
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.POST, value = "/restore")
    public ResponseEntity<MessageDto> addRestoreTask(@RequestBody TaskDto taskInfo, Principal principal) {
        taskInfo.setSchedulerName(principal.getName());
        taskInfo.setRegular(Boolean.FALSE.toString());
        taskInfo.setType(TaskEntry.TaskEntryType.RESTORE.getType());
        return new ResponseEntity(new MessageDto(taskService.createTask(taskInfo)), OK);
    }


    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(method = RequestMethod.PUT, value = "/backup/regular")
    public ResponseEntity updateTask(@RequestBody TaskDto taskInfo) {
        try {
            taskInfo.setRegular(Boolean.TRUE.toString());
            taskInfo.setType(TaskEntry.TaskEntryType.BACKUP.getType());
            taskService.updateTask(taskInfo);
            return new ResponseEntity(OK);
        } catch (EnhancedSnapshotsException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NO_CONTENT);
        }
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @RequestMapping(value = "/{taskId}", method = RequestMethod.DELETE)
    @ResponseStatus(OK)
    public void removeTask(@PathVariable String taskId) {
        taskService.removeTask(taskId);
    }
}
