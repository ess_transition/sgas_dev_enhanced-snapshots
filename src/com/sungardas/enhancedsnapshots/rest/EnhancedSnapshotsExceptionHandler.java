package com.sungardas.enhancedsnapshots.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class EnhancedSnapshotsExceptionHandler {

    private static final Logger LOG = LogManager.getLogger(EnhancedSnapshotsExceptionHandler.class);


    @ExceptionHandler(Throwable.class)
    public ResponseEntity exceptionHandler(Throwable throwable) {
        LOG.debug(throwable);
        return new ResponseEntity(throwable.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
