package com.sungardas.enhancedsnapshots.util;

import com.amazonaws.services.simplesystemsmanagement.model.InstanceInformation;


public class OSTypesUtils {
    public static boolean isLinuxPlatform(InstanceInformation instanceInformation) {
        return instanceInformation.getPlatformType().equals(OSTypes.Linux.name());
    }

    public static boolean isWindowsPlatform(InstanceInformation instanceInformation) {
        return instanceInformation.getPlatformType().equals(OSTypes.Windows.name());
    }

    public enum OSTypes {
        Linux, Windows;
    }
}
