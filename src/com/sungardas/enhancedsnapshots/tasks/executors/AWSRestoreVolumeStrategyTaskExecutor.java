package com.sungardas.enhancedsnapshots.tasks.executors;


import com.amazonaws.services.ec2.model.*;
import com.sungardas.enhancedsnapshots.aws.dynamodb.model.BackupEntry;
import com.sungardas.enhancedsnapshots.aws.dynamodb.model.TaskEntry;
import com.sungardas.enhancedsnapshots.aws.dynamodb.repository.BackupRepository;
import com.sungardas.enhancedsnapshots.aws.dynamodb.repository.TaskRepository;
import com.sungardas.enhancedsnapshots.components.ConfigurationMediator;
import com.sungardas.enhancedsnapshots.dto.CopyingTaskProgressDto;
import com.sungardas.enhancedsnapshots.enumeration.TaskProgress;
import com.sungardas.enhancedsnapshots.exception.DataAccessException;
import com.sungardas.enhancedsnapshots.exception.EnhancedSnapshotsException;
import com.sungardas.enhancedsnapshots.exception.EnhancedSnapshotsInterruptedException;
import com.sungardas.enhancedsnapshots.exception.EnhancedSnapshotsTaskInterruptedException;
import com.sungardas.enhancedsnapshots.service.*;
import com.sungardas.enhancedsnapshots.util.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.sungardas.enhancedsnapshots.aws.dynamodb.model.TaskEntry.TaskEntryStatus.*;
import static com.sungardas.enhancedsnapshots.enumeration.TaskProgress.FAIL_CLEANING;
import static com.sungardas.enhancedsnapshots.enumeration.TaskProgress.INTERRUPTED_CLEANING;

@Service("awsRestoreVolumeTaskExecutor")
public class AWSRestoreVolumeStrategyTaskExecutor extends AbstractAWSVolumeTaskExecutor {
    public static final String RESTORED_NAME_PREFIX = "Restore of ";
    private static final Logger LOG = LogManager.getLogger(AWSRestoreVolumeStrategyTaskExecutor.class);
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private BackupRepository backupRepository;

    @Autowired
    private SnapshotService snapshotService;

    @Autowired
    private AWSCommunicationService awsCommunication;

    @Autowired
    private StorageService storageService;

    @Autowired
    private ConfigurationMediator configurationMediator;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private TaskService taskService;

    @Override
    public void execute(TaskEntry taskEntry) {
        switch (taskEntry.progress()) {
            case FAIL_CLEANING: {
                failCleaningStep(taskEntry, new EnhancedSnapshotsException("Restore failed"));
                return;
            }
            case INTERRUPTED_CLEANING: {
                interruptedCleaningStep(taskEntry);
                return;
            }
        }
        try {
            LOG.info("{}: starting restore process for volume {}", taskEntry.getId(), taskEntry.getVolume());
            LOG.info("{}: task state was changed to 'in progress'", taskEntry.getId());
            taskEntry.setStatus(RUNNING.getStatus());
            taskRepository.save(taskEntry);
            notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Starting restore", 0);

            String sourceFile = taskEntry.getBackupFileName();
            if (snapshotService.getSnapshotIdByVolumeId(taskEntry.getVolume()) != null && (sourceFile == null || sourceFile.isEmpty())) {
                LOG.info("{}: task was defined as restore from snapshot.", taskEntry.getId());
                notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Restore from Snapshot", 5);
                restoreFromSnapshot(taskEntry);
            } else {
                LOG.info("{}: task was defined as restore from history.", taskEntry.getId());
                notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Restore from S3", 0);
                restoreFromBackupFile(taskEntry);
            }
            completeTask(taskEntry);
        } catch (EnhancedSnapshotsTaskInterruptedException e) {
            interruptedCleaningStep(taskEntry);
        } catch (EnhancedSnapshotsInterruptedException e) {
            if (!configurationMediator.isClusterMode()) {
                interruptedCleaningStep(taskEntry);
            }
        } catch (Exception e) {
            failCleaningStep(taskEntry, e);
        }
    }

    private void restoreFromSnapshot(TaskEntry taskEntry) {
        try {
            notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Restore from snapshot", 20);
            String targetZone = taskEntry.getAvailabilityZone();
            if (taskEntry.getInstanceToAttach() != null) {
                targetZone = awsCommunication.getInstanceStatus(taskEntry.getInstanceToAttach()).getAvailabilityZone();
                taskEntry.setAvailabilityZone(targetZone);
                taskRepository.save(taskEntry);
            }

            String volumeId = taskEntry.getVolume();
            String snapshotId = snapshotService.getSnapshotIdByVolumeId(volumeId);
            // check that snapshot exists
            if (snapshotId == null || !awsCommunication.snapshotExists(snapshotId)) {
                LOG.error("{}: failed to find snapshot for volume {} ", taskEntry.getId(), volumeId);
                throw new DataAccessException("Backup for volume: " + volumeId + " was not found");
            }

            checkThreadInterruption(taskEntry);
            notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Creating volume from snapshot", 50);

            Volume volume = awsCommunication.createVolumeFromSnapshot(snapshotId, targetZone, VolumeType.fromValue(taskEntry.getRestoreVolumeType()),
                    taskEntry.getRestoreVolumeIopsPerGb());
            awsCommunication.setResourceName(volume.getVolumeId(), RESTORED_NAME_PREFIX + taskEntry.getVolume());
            awsCommunication.addTag(volume.getVolumeId(), "Created by", "Enhanced Snapshots");
            attachToInstanceIfRequired(volume.getVolumeId(), taskEntry);
            setProgress(taskEntry, TaskProgress.DONE);
        } catch (EnhancedSnapshotsTaskInterruptedException e) {
            LOG.info("{}: restore task was canceled", taskEntry.getId());
            taskRepository.delete(taskEntry);
            notificationService.notifyAboutSystemStatus("Restore task for volume with id" + taskEntry.getVolume() + " was canceled");
            setProgress(taskEntry, TaskProgress.DONE);
        }
    }

    private void restoreFromBackupFile(TaskEntry taskEntry) throws IOException, InterruptedException {
        Volume tempVolume = taskEntry.getTempVolumeId() != null ? awsCommunication.getVolume(taskEntry.getTempVolumeId()) : null;
        BackupEntry backupEntry;
        if (taskEntry.getBackupFileName() != null && !taskEntry.getBackupFileName().isEmpty()) {
            backupEntry = backupRepository.findOne(taskEntry.getBackupFileName());
        } else {
            backupEntry = backupRepository.findByVolumeId(taskEntry.getVolume())
                    .stream().sorted((e1, e2) -> e2.getTimeCreated().compareTo(e1.getTimeCreated()))
                    .findFirst().get();
        }
        if (taskEntry.getInstanceToAttach() != null) {
            taskEntry.setAvailabilityZone(awsCommunication.getInstanceStatus(taskEntry.getInstanceToAttach()).getAvailabilityZone());
            taskRepository.save(taskEntry);
        }
        if (taskEntry.progress() != TaskProgress.NONE) {
            switch (taskEntry.progress()) {
                case ATTACHING_VOLUME:
                case CREATING_TEMP_VOLUME:
                case WAITING_TEMP_VOLUME:
                case COPYING: {
                    try {
                        notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Detaching temp volume", 50);
                        detachingTempVolumeStep(taskEntry);
                    } catch (Exception e) {
                        // skip
                    }
                    try {
                        notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Deleting temp volume", 20);
                        deletingTempVolumeStep(taskEntry);
                    } catch (Exception e) {
                        // skip
                    }
                    setProgress(taskEntry, TaskProgress.STARTED);
                    break;
                }
                case CREATING_SNAPSHOT: {
                    if (taskEntry.getTempSnapshotId() != null) {
                        setProgress(taskEntry, TaskProgress.WAITING_SNAPSHOT);
                    }
                    break;
                }
            }
        }
        switch (taskEntry.progress()) {
            case NONE:
            case STARTED:
                setProgress(taskEntry, TaskProgress.STARTED);
            case CREATING_TEMP_VOLUME: {
                notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Creating temp volume", 10);
                creationTempVolumeStep(taskEntry, backupEntry);
            }
            case WAITING_TEMP_VOLUME: {
                notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Waiting temp volume", 15);
                tempVolume = waitingTempVolumeStep(taskEntry);
            }
            case ATTACHING_VOLUME: {
                notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Attaching temp volume", 20);
                tempVolume = attachingVolumeStep(taskEntry, tempVolume);
            }
            case COPYING: {
                copyingStep(taskEntry, tempVolume, backupEntry);
            }
            case DETACHING_TEMP_VOLUME: {
                notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Detaching temp volume", 65);
                detachingTempVolumeStep(taskEntry);
            }
        }
        if (!tempVolume.getAvailabilityZone().equals(taskEntry.getAvailabilityZone())) {
            //move to target availability zone
            switch (taskEntry.progress()) {
                case DETACHING_TEMP_VOLUME:
                case CREATING_SNAPSHOT: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Creating snapshot", 70);
                    creatingTempSnapshotStep(taskEntry);
                }
                case WAITING_SNAPSHOT: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Waiting snapshot", 75);
                    waitingTempSnapshotStep(taskEntry);
                }
                case MOVE_TO_TARGET_ZONE: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Moving to target Zone", 80);
                    Volume volumeToRestore = moveToTargetZoneStep(taskEntry);
                    awsCommunication.setResourceName(volumeToRestore.getVolumeId(), RESTORED_NAME_PREFIX + backupEntry.getFileName());
                    addTags(volumeToRestore.getVolumeId(), backupEntry);
                    attachToInstanceIfRequired(volumeToRestore.getVolumeId(), taskEntry);
                }
                case DELETING_TEMP_VOLUME: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Deleting temp volume", 85);
                    deletingTempVolumeStep(taskEntry);
                }
                case DELETING_TEMP_SNAPSHOT: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Deleting temp snapshot", 90);
                    deletingTempSnapshotStep(taskEntry);
                }
            }
        } else {
            //in case availability zone is the same we do not need temp volume
            awsCommunication.deleteTemporaryTag(tempVolume.getVolumeId());
            awsCommunication.setResourceName(tempVolume.getVolumeId(), RESTORED_NAME_PREFIX + backupEntry.getFileName());
            addTags(tempVolume.getVolumeId(), backupEntry);
            attachToInstanceIfRequired(tempVolume.getVolumeId(), taskEntry);
        }
    }

    private void attachToInstanceIfRequired(String volumeId, TaskEntry taskEntry) {
        try {
            if (taskEntry.getInstanceToAttach() != null) {
                LOG.info("{}: attaching to instance {}", taskEntry.getId(), taskEntry.getInstanceToAttach());
                notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Attaching to instance", 85);
                awsCommunication.attachVolume(awsCommunication.getInstance(taskEntry.getInstanceToAttach()),
                        awsCommunication.getVolume(volumeId));
            }
        } catch (Exception e) {
            LOG.error("{}: failed to attache volume {} to instance {}", taskEntry.getId(), volumeId, taskEntry.getInstanceToAttach());
            LOG.error(e);
        }
    }

    private Volume creationTempVolumeStep(TaskEntry taskEntry, BackupEntry backupEntry) {
        checkThreadInterruption(taskEntry);
        setProgress(taskEntry, TaskProgress.CREATING_TEMP_VOLUME);
        Volume tempVolume;

        LOG.info("{}: used backup record {}", taskEntry.getId(), backupEntry.getFileName());
        int size = Integer.parseInt(backupEntry.getSizeGiB());
        checkThreadInterruption(taskEntry);

        String volumeType;
        int volumeIopsPerGb;

        if (SystemUtils.getAvailabilityZone().equals(taskEntry.getAvailabilityZone())) {
            volumeType = taskEntry.getRestoreVolumeType();
            volumeIopsPerGb = taskEntry.getRestoreVolumeIopsPerGb();
        } else {
            volumeType = taskEntry.getTempVolumeType();
            volumeIopsPerGb = taskEntry.getTempVolumeIopsPerGb();
        }

        // creating temporary volume
        if (volumeType.equals(VolumeType.Io1.toString())) {
            tempVolume = awsCommunication.createIO1Volume(size, volumeIopsPerGb);
        } else {
            tempVolume = awsCommunication.createVolume(size, VolumeType.fromValue(volumeType));
        }
        LOG.info("{}: created {} volume {}", taskEntry.getId(), volumeType, tempVolume.toString());
        checkThreadInterruption(taskEntry);
        awsCommunication.createTemporaryTag(tempVolume.getVolumeId(), backupEntry.getFileName());
        taskEntry.setTempVolumeId(tempVolume.getVolumeId());
        taskRepository.save(taskEntry);

        return tempVolume;
    }

    private Volume waitingTempVolumeStep(TaskEntry taskEntry) {
        checkThreadInterruption(taskEntry);
        setProgress(taskEntry, TaskProgress.WAITING_TEMP_VOLUME);
        Volume volumeDest = awsCommunication.waitForVolumeState(awsCommunication.getVolume(taskEntry.getTempVolumeId()), VolumeState.Available);
        LOG.info("{}: volume created {}", taskEntry.getId(), volumeDest.toString());
        return volumeDest;
    }

    private Volume attachingVolumeStep(TaskEntry taskEntry, Volume tempVolume) {
        checkThreadInterruption(taskEntry);
        setProgress(taskEntry, TaskProgress.ATTACHING_VOLUME);
        awsCommunication.attachVolume(awsCommunication.getInstance(SystemUtils.getInstanceId()), tempVolume);

        tempVolume = awsCommunication.syncVolume(tempVolume);

        try {
            TimeUnit.MINUTES.sleep(1);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        checkThreadInterruption(taskEntry);
        return tempVolume;
    }

    private void copyingStep(TaskEntry taskEntry, Volume tempVolume, BackupEntry backupEntry) throws IOException, InterruptedException {
        String attachedDeviceName = storageService.detectFsDevName(tempVolume);
        LOG.info("{}: volume was attached as device {}.", taskEntry.getId(), attachedDeviceName);
        CopyingTaskProgressDto dto = new CopyingTaskProgressDto(taskEntry.getId(), 25, 60, Long.parseLong(backupEntry.getSizeGiB()));
        storageService.copyData(configurationMediator.getSdfsMountPoint() + backupEntry.getFileName(), attachedDeviceName, dto, taskEntry.getId());
    }

    private void completeTask(TaskEntry taskEntry) {
        notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Restore complete", 100);
        taskEntry.setProgress(TaskProgress.DONE.name());
        taskService.complete(taskEntry);
        LOG.info("{}: task {} was completed", taskEntry.getType(), taskEntry.getId());
        if(taskEntry.getStatus().equals(COMPLETE_WITH_WARNINGS.getStatus())) {
            notificationService.notifyViaSns(TaskEntry.TaskEntryType.RESTORE, COMPLETE_WITH_WARNINGS, taskEntry.getVolume());
        } else {
            notificationService.notifyViaSns(TaskEntry.TaskEntryType.RESTORE, COMPLETE, taskEntry.getVolume());
        }
        notificationService.notifyAboutSuccess(taskEntry);
    }


    private void detachingTempVolumeStep(TaskEntry taskEntry) {
        setProgress(taskEntry, TaskProgress.DETACHING_TEMP_VOLUME);
        checkThreadInterruption(taskEntry);
        LOG.info("{}: detaching volume {}", taskEntry.getId(), taskEntry.getTempVolumeId());
        awsCommunication.detachVolume(taskEntry.getTempVolumeId());
    }

    private void creatingTempSnapshotStep(TaskEntry taskEntry) {
        setProgress(taskEntry, TaskProgress.CREATING_SNAPSHOT);
        Volume volumeSrc = awsCommunication.getVolume(taskEntry.getTempVolumeId());
        if (volumeSrc == null) {
            LOG.error("{}: can't get access to {} volume", taskEntry.getId(), taskEntry.getTempVolumeId());
            throw new DataAccessException(MessageFormat.format("Can't get access to {} volume", taskEntry.getVolume()));
        }
        taskEntry.setTempSnapshotId(awsCommunication.createSnapshot(volumeSrc).getSnapshotId());
        taskRepository.save(taskEntry);
    }

    private void waitingTempSnapshotStep(TaskEntry taskEntry) {
        setProgress(taskEntry, TaskProgress.WAITING_SNAPSHOT);
        Snapshot snapshot = awsCommunication.waitForCompleteState(awsCommunication.getSnapshot(taskEntry.getTempSnapshotId()));
        LOG.info("{}: snapshotEntry created {}", taskEntry.getId(), snapshot.toString());
    }

    private Volume moveToTargetZoneStep(TaskEntry taskEntry) {
        checkThreadInterruption(taskEntry);
        notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Moving into target zone...", 95);

        return awsCommunication.createVolumeFromSnapshot(taskEntry.getTempSnapshotId(), taskEntry.getAvailabilityZone(),
                VolumeType.fromValue(taskEntry.getRestoreVolumeType()), taskEntry.getRestoreVolumeIopsPerGb());
    }

    private void deletingTempVolumeStep(TaskEntry taskEntry) {
        setProgress(taskEntry, TaskProgress.DELETING_TEMP_VOLUME);
        LOG.info("{}: deleting temporary volume {}", taskEntry.getId(), taskEntry.getTempVolumeId());
        awsCommunication.deleteVolume(taskEntry.getTempVolumeId());
    }

    private void deletingTempSnapshotStep(TaskEntry taskEntry) {
        setProgress(taskEntry, TaskProgress.DELETING_TEMP_SNAPSHOT);
        LOG.info("{}: deleting temporary snapshot: {}", taskEntry.getId(), taskEntry.getTempSnapshotId());
        awsCommunication.deleteSnapshot(taskEntry.getTempSnapshotId());
    }

    private void cleaningStep(TaskEntry taskEntry) {
        try {
            deletingTempSnapshotStep(taskEntry);
        } catch (Exception e) {
            //skip
        }
        try {
            deletingTempVolumeStep(taskEntry);
        } catch (Exception e) {
            //skip
        }

    }

    private void interruptedCleaningStep(TaskEntry taskEntry) {
        setProgress(taskEntry, INTERRUPTED_CLEANING);
        cleaningStep(taskEntry);
        LOG.info("{}: restore task was canceled", taskEntry.getId());
        taskRepository.delete(taskEntry);
        notificationService.notifyAboutSystemStatus("Restore task for volume with id" + taskEntry.getVolume() + " was canceled");
    }

    private void failCleaningStep(TaskEntry taskEntry, Exception e) {
        setProgress(taskEntry, FAIL_CLEANING);
        cleaningStep(taskEntry);
        LOG.error("{}: failed to execute {} task {}. Changing task status to '{}'", taskEntry.getId(), taskEntry.getType(), taskEntry.getId(), TaskEntry.TaskEntryStatus.ERROR);
        LOG.error(e);
        taskEntry.setStatus(TaskEntry.TaskEntryStatus.ERROR.getStatus());
        taskRepository.save(taskEntry);
        notificationService.notifyViaSns(TaskEntry.TaskEntryType.RESTORE, ERROR, taskEntry.getVolume());
        notificationService.notifyAboutError(taskEntry, e);
    }

    private void addTags(String resourceId, BackupEntry backupEntry) {
        List<Tag> tags = backupEntry.getTags();
        if (tags != null) {
            tags.add(new Tag("Created by", "Enhanced Snapshots"));
            awsCommunication.addTag(resourceId, tags);
        } else {
            awsCommunication.addTag(resourceId, "Created by", "Enhanced Snapshots");
        }
    }
}
