package com.sungardas.enhancedsnapshots.tasks.executors;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.services.simplesystemsmanagement.model.CommandInvocation;
import com.amazonaws.services.simplesystemsmanagement.model.InstanceInformation;
import com.amazonaws.services.simplesystemsmanagement.model.SendCommandRequest;
import com.sungardas.enhancedsnapshots.aws.dynamodb.model.BackupEntry;
import com.sungardas.enhancedsnapshots.aws.dynamodb.model.BackupState;
import com.sungardas.enhancedsnapshots.aws.dynamodb.model.TaskEntry;
import com.sungardas.enhancedsnapshots.aws.dynamodb.repository.BackupRepository;
import com.sungardas.enhancedsnapshots.aws.dynamodb.repository.TaskRepository;
import com.sungardas.enhancedsnapshots.components.ConfigurationMediator;
import com.sungardas.enhancedsnapshots.dto.CopyingTaskProgressDto;
import com.sungardas.enhancedsnapshots.dto.TaskProgressDto;
import com.sungardas.enhancedsnapshots.dto.converter.VolumeDtoConverter;
import com.sungardas.enhancedsnapshots.enumeration.TaskProgress;
import com.sungardas.enhancedsnapshots.exception.EnhancedSnapshotsException;
import com.sungardas.enhancedsnapshots.exception.EnhancedSnapshotsInterruptedException;
import com.sungardas.enhancedsnapshots.exception.EnhancedSnapshotsTaskInterruptedException;
import com.sungardas.enhancedsnapshots.service.*;
import com.sungardas.enhancedsnapshots.util.OSTypesUtils;
import com.sungardas.enhancedsnapshots.util.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.sungardas.enhancedsnapshots.aws.dynamodb.model.TaskEntry.TaskEntryStatus.*;

@Service("awsBackupVolumeTaskExecutor")
public class AWSBackupVolumeStrategyTaskExecutor extends AbstractAWSVolumeTaskExecutor {

    private static final Logger LOG = LogManager.getLogger(AWSBackupVolumeStrategyTaskExecutor.class);
    // aws ssm document names
    private static final String FS_FREEZE_LINUX_DOCUMENT_DEFAULT_NAME = "ESSLinuxFreeze";
    private String fs_freeze_linux_document_name = null;
    private static final String CREATE_VSS_SNAPSHOT_WINDOWS_DOCUMENT_DEFAULT_NAME = "ESSWindowsCreateVSS";
    private String create_vss_snapshot_windows_document_name = null;
    private static final String FS_UNFREEZE_LINUX_DOCUMENT_DEFAULT_NAME = "ESSLinuxUnFreeze";
    private String fs_unfreeze_linux_document_name = null;
    private static final String REMOVE_VSS_SNAPSHOT_WINDOWS_DOCUMENT_DEFAULT_NAME = "ESSWindowsDeleteVSS";
    private String remove_vss_snapshot_windows_document_name = null;
    private static final String ESS_HEALTH_CHECK_DOCUMENT_DEFAULT_NAME = "ESSCheckApp";
    private String ess_health_check_document_name = null;

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private StorageService storageService;
    @Autowired
    private BackupRepository backupRepository;
    @Autowired
    private BackupService backupService;
    @Autowired
    private SnapshotService snapshotService;
    @Autowired
    private AWSCommunicationService awsCommunication;
    @Autowired
    private ConfigurationMediator configurationMediator;
    @Autowired
    private RetentionService retentionService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private VolumeService volumeService;

    @Value("${enhancedsnapshots.default.max.wait.time.to.freeze.fs}")
    private String maxWaitTimeToFreezeFSWhenAppIsUnavailable;

    private String instanceId = SystemUtils.getInstanceId();


    @Override
    public void execute(final TaskEntry taskEntry) {
        Volume tempVolume = taskEntry.getTempVolumeId() != null ? awsCommunication.getVolume(taskEntry.getTempVolumeId()) : null;
        try {
            if (taskEntry.getProgress().equals(TaskProgress.NONE) && taskEntry.isConsistentBackup()) {
                if (taskEntry.isConsistentBackup() && taskEntry.isConsistentBackup() != backupService.consistentBackupSupported(taskEntry.getVolume())) {
                    taskEntry.setWarning("Consistent backup is not supported.");
                    taskEntry.setConsistentBackup(false);
                    taskRepository.save(taskEntry);
                }
            }
            LOG.info("{}: Starting backup process for volume {}", taskEntry.getId(), taskEntry.getVolume());
            LOG.info("{}: Task state was changed to 'in progress'", taskEntry.getId());
            // change task status
            taskEntry.setStatus(RUNNING.getStatus());
            taskRepository.save(taskEntry);

            if (taskEntry.progress() != TaskProgress.NONE) {
                switch (taskEntry.progress()) {
                    case CREATING_TEMP_VOLUME:
                    case WAITING_TEMP_VOLUME:
                    case ATTACHING_VOLUME:
                    case COPYING: {
                        try {
                            notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Detaching temp volume", 85);
                            detachingTempVolumeStep(taskEntry);
                        } catch (Exception e) {
                            //skip
                        }
                        try {
                            notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Deleting temp volume", 50);
                            deletingTempVolumeStep(taskEntry);
                        } catch (Exception e) {
                            //skip
                        }
                        try {
                            notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Deleting temp file", 10);
                            storageService.deleteFile(getBackupName(taskEntry));
                        } catch (Exception e) {
                            //skip
                        }
                        setProgress(taskEntry, TaskProgress.CREATING_TEMP_VOLUME);
                        break;
                    }
                    case CREATING_SNAPSHOT: {
                        if (taskEntry.getTempSnapshotId() != null) {
                            setProgress(taskEntry, TaskProgress.WAITING_SNAPSHOT);
                        }
                        break;
                    }
                    case DETACHING_TEMP_VOLUME: {
                        setProgress(taskEntry, TaskProgress.DELETING_TEMP_VOLUME);
                    }
                }
            }
            switch (taskEntry.progress()) {
                case INTERRUPTED_CLEANING: {
                    interruptedCleaningStep(taskEntry, tempVolume);
                    break;
                }
                case FAIL_CLEANING: {
                    failCleaningStep(taskEntry, tempVolume, null);
                    break;
                }

                case NONE: {
                    // check volume exists
                    if (!volumeService.volumeExists(taskEntry.getVolume())) {
                        LOG.info("{}: Volume {} does not exist. Removing backup task.", taskEntry.getId(), taskEntry.getVolume());
                        taskRepository.delete(taskEntry);
                        return;
                    }
                }
                case STARTED: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Starting backup task", 0);
                    startedStep(taskEntry);
                }
                case CREATING_SNAPSHOT: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Creating snapshot", 5);
                    creatingSnapshotStep(taskEntry);
                }
                case CREATING_TEMP_VOLUME: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Creating temp volume", 15);
                    tempVolume = creatingTempVolumeStep(taskEntry);
                }
                case WAITING_TEMP_VOLUME: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Waiting temp volume", 20);
                    waitingTempVolumeStep(taskEntry, tempVolume);
                }
                case ATTACHING_VOLUME: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Attaching volume", 25);
                    tempVolume = attachingVolumeStep(taskEntry, tempVolume);
                }
                case COPYING: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Copying ...", 30);
                    copyingStep(taskEntry, tempVolume);
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Backup complete", 80);
                }
                case DETACHING_TEMP_VOLUME: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Detaching temp volume", 85);
                    detachingTempVolumeStep(taskEntry);
                }
                case DELETING_TEMP_VOLUME: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Deleting temp volume", 90);
                    deletingTempVolumeStep(taskEntry);
                }
                case CLEANING_TEMP_RESOURCES: {
                    notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Cleaning temp resources", 95);
                    cleaningTempResourcesStep(taskEntry);
                }
            }
            notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), taskEntry.getWarning() == null ? "Complete" : "Complete, with warning: " + taskEntry.getWarning(), 100);
            setProgress(taskEntry, TaskProgress.DONE);
        } catch (EnhancedSnapshotsTaskInterruptedException e) {
            // Throws in case task was canceled.
            interruptedCleaningStep(taskEntry, tempVolume);
        } catch (EnhancedSnapshotsInterruptedException e) {
            if (!configurationMediator.isClusterMode()) {
                interruptedCleaningStep(taskEntry, tempVolume);
            }
            setProgress(taskEntry, TaskProgress.DONE);
        } catch (Exception e) {
            failCleaningStep(taskEntry, tempVolume, e);
            setProgress(taskEntry, TaskProgress.DONE);
        }
    }

    // in ESS 4.0 officially supported for RHEL 6.5 and 7
    private void freezeLinuxFileSystem(TaskEntry taskEntry, String instanceId) {
        try {
            String deviceName = awsCommunication.getDeviceNameOfVolume(taskEntry.getVolume());
            Map<String, List<String>> params = new HashMap<>();
            params.put("device", Arrays.asList(deviceName));
            params.put("appURL", Arrays.asList("https://" + awsCommunication.getInstance(SystemUtils.getInstanceId()).getPublicDnsName()));
            params.put("timeout", Arrays.asList(maxWaitTimeToFreezeFSWhenAppIsUnavailable));
            params.put("instanceId", Arrays.asList(SystemUtils.getInstanceId()));
            params.put("documentName", Arrays.asList(getEssHealthCheckDocumentName()));
            params.put("region", Arrays.asList(Regions.getCurrentRegion().getName()));
            LOG.info("{}: Freezing FS for device {} on instance {}", taskEntry.getId(), deviceName, instanceId);
            notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Freezing FS", 6);
            CommandInvocation commandInvocation = awsCommunication.executeCommandWithSSM(new SendCommandRequest()
                    .withInstanceIds(instanceId)
                    .withParameters(params)
                    .withDocumentName(getFsFreezeLinuxDocument()));

            if (getPropertyFromCommandOutput(commandInvocation, "EXIT_RESULT").equals("Frozen")) {
                LOG.info("{}: FS for device {} frozen.", taskEntry.getId(), deviceName);
                return;
            }
        } catch (Exception e) {
            LOG.error(taskEntry.getId(), e);
        }
        LOG.warn("Failed to freeze fs");
        setWarningAndBackupInconsistency(taskEntry, "Failed to freeze FS. Consistency is not guaranteed");
    }

    private String getPropertyFromCommandOutput(CommandInvocation invocation, String propertyName) {
        try {
            Properties properties = new Properties();
            properties.load(new StringReader(invocation.getCommandPlugins().get(0).getOutput()));
            return properties.getProperty(propertyName);
        } catch (Exception e) {
            LOG.warn("Failed to load properties", e);
            return null;
        }
    }

    private void unfreezeLinuxFileSystem(TaskEntry taskEntry, String instanceId) {
        try {
            String deviceName = awsCommunication.getDeviceNameOfVolume(taskEntry.getVolume());
            Map<String, List<String>> params = new HashMap<>();
            params.put("device", Arrays.asList(deviceName));
            LOG.info("{}: Unfreezing FS for device {} on instance {}", taskEntry.getId(), deviceName, instanceId);
            CommandInvocation commandInvocation = awsCommunication.executeCommandWithSSM(new SendCommandRequest()
                    .withInstanceIds(instanceId)
                    .withParameters(params)
                    .withDocumentName(getFsUnfreezeLinuxDocument()));
            if (getPropertyFromCommandOutput(commandInvocation, "EXIT_RESULT").equals("Unfrozen")) {
                LOG.info("{}: FS for device {} unfrozen.", taskEntry.getId(), deviceName);
                return;
            }
        } catch (Exception e) {
            LOG.error(taskEntry.getId(), e);
        }
        LOG.warn("{}: Failed to unfreeze FS. Consistency can not be guaranteed", taskEntry.getId());
        setWarningAndBackupInconsistency(taskEntry, "Failed to freeze FS. Consistency is not guaranteed");
    }

    private void setWarningAndBackupInconsistency(TaskEntry taskEntry, String warning) {
        taskEntry.setConsistentBackup(false);
        taskEntry.setWarning(warning);
        taskRepository.save(taskEntry);
    }

    private void interruptedCleaningStep(TaskEntry taskEntry, Volume tempVolume) {
        LOG.info("{}: Backup process for volume {} canceled ", taskEntry.getId(), taskEntry.getVolume());
        setProgress(taskEntry, TaskProgress.INTERRUPTED_CLEANING);
        TaskProgressDto dto = new TaskProgressDto(taskEntry.getId(), "Kill initialization process", 20, CANCELED);
        notificationService.notifyAboutTaskProgress(dto);
        // kill initialization Process if it's alive, should be killed before volume detaching
        try {
            killInitializationVolumeProcessIfAlive(taskEntry);
        } catch (Exception e) {
            LOG.error("{}: Killing initialization process failed", taskEntry.getId(), e);
        }
        dto = new TaskProgressDto(taskEntry.getId(), "Deleting temp resources", 50, CANCELED);
        notificationService.notifyAboutTaskProgress(dto);
        try {
            deleteTempResources(tempVolume, getBackupName(taskEntry), dto);
        } catch (Exception e) {
            LOG.error("{}: Deleting temp resources failed", taskEntry.getId(), e);
        }
        taskEntry.setProgress(TaskProgress.DONE.name());
        taskRepository.delete(taskEntry);

        notificationService.notifyAboutTaskProgress(dto);
        String instanceId = awsCommunication.getInstanceVolumeBelongsTo(taskEntry.getVolume());
        if (taskEntry.isConsistentBackup() && instanceId != null && awsCommunication.instanceIsRunning(instanceId)) {
            if (OSTypesUtils.isWindowsPlatform(awsCommunication.getSSMInstanceInformation(instanceId))) {
                cleanupVssSnapshot(taskEntry, instanceId);
            } else {
                unfreezeLinuxFileSystem(taskEntry, instanceId);
            }
        }
        dto.setProgress(100);
        dto.setMessage("Done");
        notificationService.notifyAboutSystemStatus("Backup task for volume with id: " + taskEntry.getVolume() + " was canceled");
    }

    private void failCleaningStep(TaskEntry taskEntry, Volume tempVolume, Exception e) {
        LOG.error("{}: Backup process for volume {} failed ", taskEntry.getId(), taskEntry.getVolume(), e);
        setProgress(taskEntry, TaskProgress.FAIL_CLEANING);
        taskEntry.setStatus(ERROR.toString());
        taskRepository.save(taskEntry);

        TaskProgressDto dto = new TaskProgressDto(taskEntry.getId(), "Killing initialization process", 20, ERROR);
        notificationService.notifyAboutTaskProgress(dto);

        // kill initialization Process if it's alive, should be killed before volume detaching
        try {
            killInitializationVolumeProcessIfAlive(taskEntry);
        } catch (Exception e1) {
            LOG.error("{}: Killing initialization process failed", taskEntry.getId(), e1);
        }
        try {
            deleteTempResources(tempVolume, getBackupName(taskEntry), dto);
        } catch (Exception e1) {
            LOG.error("{}: Delete temp resources failed", taskEntry.getId());
        }
        dto.setMessage("Done");
        dto.setProgress(100);
        notificationService.notifyAboutTaskProgress(dto);
        notificationService.notifyViaSns(TaskEntry.TaskEntryType.BACKUP, TaskEntry.TaskEntryStatus.ERROR, taskEntry.getVolume());
        notificationService.notifyAboutError(taskEntry, e);
    }

    private void startedStep(TaskEntry taskEntry) {
        setProgress(taskEntry, TaskProgress.STARTED);
        checkThreadInterruption(taskEntry);
        taskEntry.setStartTime(System.currentTimeMillis());
    }

    private void creatingSnapshotStep(TaskEntry taskEntry) {
        checkThreadInterruption(taskEntry);
        if (taskEntry.getTempSnapshotId() != null) {
            // for multi-node mode
            // we can not be sure that created snapshot by failed node is consistent
            if (taskEntry.isConsistentBackup()) {
                awsCommunication.deleteSnapshot(taskEntry.getTempSnapshotId());
                taskEntry.setTempSnapshotId(null);
            } else {
                awsCommunication.waitForCompleteState(awsCommunication.getSnapshot(taskEntry.getTempSnapshotId()));
                return;
            }
        }
        InstanceInformation ssmInstanceInformation = null;
        String instanceId = awsCommunication.getInstanceVolumeBelongsTo(taskEntry.getVolume());
        // only for consistent backups
        if (taskEntry.isConsistentBackup() && instanceId != null) {
            ssmInstanceInformation = awsCommunication.getSSMInstanceInformation(instanceId);
            if (OSTypesUtils.isWindowsPlatform(ssmInstanceInformation)) {
                createVSSSnapshot(taskEntry, instanceId);
            } else {
                freezeLinuxFileSystem(taskEntry, instanceId);
            }
        }
        setProgress(taskEntry, TaskProgress.CREATING_SNAPSHOT);
        Volume volumeSrc = awsCommunication.getVolume(taskEntry.getVolume());
        taskEntry.setTags(volumeSrc.getTags());
        if (volumeSrc == null) {
            LOG.error("{}: Can't get access to {} volume", taskEntry.getId(), taskEntry.getVolume());
            throw new AWSBackupVolumeException(MessageFormat.format("Can't get access to {} volume", taskEntry.getVolume()));
        }
        Snapshot snapshot = awsCommunication.createSnapshot(volumeSrc);
        taskEntry.setTempSnapshotId(snapshot.getSnapshotId());
        taskRepository.save(taskEntry);
        checkThreadInterruption(taskEntry);
        awsCommunication.waitForCompleteState(awsCommunication.getSnapshot(snapshot.getSnapshotId()));
        checkThreadInterruption(taskEntry);
        LOG.info("{}: SnapshotEntry created: {}", taskEntry.getId(), snapshot.toString());
        // only for consistent backups
        if (taskEntry.isConsistentBackup() && instanceId != null) {
            if (OSTypesUtils.isWindowsPlatform(ssmInstanceInformation)) {
                notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Removing VSS snapshot", 8);
                cleanupVssSnapshot(taskEntry, instanceId);
            } else {
                notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Unfreezing FS", 7);
                unfreezeLinuxFileSystem(taskEntry, instanceId);
            }
        }
    }

    // stores ids of created vss snapshots in format: {id1} {id2}
    private void createVSSSnapshot(TaskEntry taskEntry, String instanceId) {
        try {
            String deviceName = awsCommunication.getDeviceNameOfVolume(taskEntry.getVolume());
            Map<String, List<String>> params = new HashMap<>();
            params.put("device", Arrays.asList(deviceName));
            LOG.info("{}: Creating VSS snapshot for device {} on instance {}", taskEntry.getId(), deviceName, instanceId);
            notificationService.notifyAboutRunningTaskProgress(taskEntry.getId(), "Creating VSS snapshot", 6);
            CommandInvocation commandInvocation = awsCommunication.executeCommandWithSSM(new SendCommandRequest()
                    .withInstanceIds(instanceId)
                    .withParameters(params)
                    .withDocumentName(getCreateVssSnapshotWindowsDocument()));
            if (getPropertyFromCommandOutput(commandInvocation, "EXIT_RESULT").equals("Frozen")) {
                LOG.info("{}: VSS snapshot for device {} created.", taskEntry.getId(), deviceName);
                taskEntry.setVssIds(getPropertyFromCommandOutput(commandInvocation, "SNAPSHOT_ID"));
                return;
            }
        } catch (Exception e) {
            LOG.error(taskEntry.getId(), e);
        }
        LOG.warn("{}: Failed to create VSS snapshot. Consistency can not be guaranteed", taskEntry.getId());
        setWarningAndBackupInconsistency(taskEntry, "Failed to create VSS snapshot. Consistency is not guaranteed");
    }

    // for windows we need to remove created vss snapshot after taking amazon snapshot
    private void cleanupVssSnapshot(TaskEntry taskEntry, String instanceId) {
        try {
            Map<String, List<String>> params = new HashMap<>();
            params.put("vssIds", Arrays.asList(taskEntry.getVssIds()));
            CommandInvocation commandInvocation = awsCommunication.executeCommandWithSSM(new SendCommandRequest()
                    .withInstanceIds(instanceId)
                    .withParameters(params)
                    .withDocumentName(getRemoveVssSnapshotWindowsDocument()));
            if (getPropertyFromCommandOutput(commandInvocation, "EXIT_RESULT").equals("Success")) {
                LOG.info("{}: Temporal VSS snapshot removed.", taskEntry.getId());
                return;
            }
        } catch (Exception e) {
            LOG.error(taskEntry.getId(), e);
        }
        LOG.warn("{}: Failed to remove temporal VSS snapshot.", taskEntry.getId());
    }

    private Volume creatingTempVolumeStep(TaskEntry taskEntry) {
        // create volume
        checkThreadInterruption(taskEntry);
        setProgress(taskEntry, TaskProgress.CREATING_TEMP_VOLUME);
        Instance instance = awsCommunication.getInstance(instanceId);
        if (instance == null) {
            LOG.error("{}: Can't get access to {} instance", taskEntry.getId(), instanceId);
            throw new AWSBackupVolumeException(MessageFormat.format("Can't get access to {} instance", instanceId));
        }
        String instanceAvailabilityZone = instance.getPlacement().getAvailabilityZone();
        Volume tempVolume = awsCommunication.createVolumeFromSnapshot(taskEntry.getTempSnapshotId(), instanceAvailabilityZone,
                VolumeType.fromValue(taskEntry.getTempVolumeType()), taskEntry.getTempVolumeIopsPerGb());
        taskEntry.setTempVolumeId(tempVolume.getVolumeId());
        taskRepository.save(taskEntry);

        // create temporary tag
        awsCommunication.createTemporaryTag(taskEntry.getTempVolumeId(), taskEntry.getVolume());
        return tempVolume;
    }

    private void waitingTempVolumeStep(TaskEntry taskEntry, Volume tempVolume) {
        checkThreadInterruption(taskEntry);
        setProgress(taskEntry, TaskProgress.WAITING_TEMP_VOLUME);
        Volume volumeDest = awsCommunication.waitForVolumeState(tempVolume, VolumeState.Available);
        LOG.info("{}: Volume created: {}", taskEntry.getId(), volumeDest.toString());
    }

    private Volume attachingVolumeStep(TaskEntry taskEntry, Volume tempVolume) {
        checkThreadInterruption(taskEntry);
        setProgress(taskEntry, TaskProgress.ATTACHING_VOLUME);
        // mount volume
        awsCommunication.attachVolume(awsCommunication.getInstance(instanceId), tempVolume);

        tempVolume = awsCommunication.syncVolume(tempVolume);

        try {
            TimeUnit.MINUTES.sleep(1);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        // storage blocks on volumes that were restored from snapshots must be initialized (pulled down from Amazon S3 and written to the volume) before they can be accesed.
        // This preliminary action takes time and can cause a significant increase in the latency of an I/O operation the first time each block is accessed.
        // To avoid this performance hit in a production environment all blocks on volumes can be read before volume usage; this process is called initialization (formerly known as pre-warming).
        initializeVolume(taskEntry);
        checkThreadInterruption(taskEntry);
        return tempVolume;
    }

    private void copyingStep(TaskEntry taskEntry, Volume tempVolume) throws IOException, InterruptedException {
        setProgress(taskEntry, TaskProgress.COPYING);
        checkThreadInterruption(taskEntry);

        Volume volumeToBackup = awsCommunication.getVolume(taskEntry.getVolume());
        String volumeType = volumeToBackup.getVolumeType();
        String iops = (volumeToBackup.getIops() != null) ? volumeToBackup
                .getIops().toString() : "";
        String sizeGib = tempVolume.getSize().toString();
        if (volumeType.equals("")) {
            volumeType = "gp2";
        }
        if (volumeType.equals("standard")) {
            volumeType = "gp2";
        }
        String backupFileName = getBackupName(taskEntry);
        BackupEntry backup = new BackupEntry(taskEntry.getVolume(),
                VolumeDtoConverter.convert(awsCommunication.getVolume(taskEntry.getVolume())).getVolumeName(),
                backupFileName, taskEntry.getStartTime() + "", "", BackupState.INPROGRESS, taskEntry.getTempSnapshotId(), volumeType, iops, sizeGib);
        checkThreadInterruption(taskEntry);
        String source = storageService.detectFsDevName(tempVolume);
        LOG.info("{}: Starting copying {} to {}", taskEntry.getId(), source, backupFileName);
        CopyingTaskProgressDto dto = new CopyingTaskProgressDto(taskEntry.getId(), 30, 80, Long.parseLong(backup.getSizeGiB()));
        storageService.copyData(source, configurationMediator.getSdfsMountPoint() + backupFileName, dto, taskEntry.getId());

        checkThreadInterruption(taskEntry);
        long backupSize = storageService.getSize(configurationMediator.getSdfsMountPoint() + backupFileName);
        long backupCreationtime = storageService.getBackupCreationTime(configurationMediator.getSdfsMountPoint() + backupFileName);
        LOG.info("{}: Backup creation time: {}", taskEntry.getId(), backupCreationtime);
        LOG.info("{}: Backup size: {}", taskEntry.getId(), backupSize);

        checkThreadInterruption(taskEntry);
        LOG.info("{}: Put backup entry to the Backup List: {}", taskEntry.getId(), backup.getFileName());
        backup.setState(BackupState.COMPLETED.getState());
        backup.setSize(String.valueOf(backupSize));
        backup.setTags(taskEntry.getTags());
        backupRepository.save(backup);

        LOG.info("{}: Backup process for volume {} finished successfully.", taskEntry.getId(), taskEntry.getVolume());
        LOG.info("{}: Cleaning up previously created snapshots", taskEntry.getId());
    }

    private void detachingTempVolumeStep(TaskEntry taskEntry) {
        setProgress(taskEntry, TaskProgress.DETACHING_TEMP_VOLUME);
        checkThreadInterruption(taskEntry);
        // kill initialization Process if it's alive, should be killed before volume detaching
        killInitializationVolumeProcessIfAlive(taskEntry);
        LOG.info("{}: Detaching volume: {}", taskEntry.getId(), taskEntry.getTempVolumeId());
        awsCommunication.detachVolume(taskEntry.getTempVolumeId());
    }

    private void deletingTempVolumeStep(TaskEntry taskEntry) {
        setProgress(taskEntry, TaskProgress.DELETING_TEMP_VOLUME);
        LOG.info("{}: Deleting temporary volume: {}", taskEntry.getId(), taskEntry.getTempVolumeId());
        awsCommunication.deleteVolume(taskEntry.getTempVolumeId());
    }

    private void cleaningTempResourcesStep(TaskEntry taskEntry) {
        setProgress(taskEntry, TaskProgress.CLEANING_TEMP_RESOURCES);
        String previousSnapshot = snapshotService.getSnapshotIdByVolumeId(taskEntry.getVolume());
        if (previousSnapshot != null) {
            checkThreadInterruption(taskEntry);
            LOG.info("{}: Deleting previous snapshot {}", taskEntry.getId(), previousSnapshot);
            snapshotService.deleteSnapshot(previousSnapshot);
        }
        if (configurationMediator.isStoreSnapshot()) {
            LOG.info("{}: Storing snapshot data: [{},{}]", taskEntry.getId(), taskEntry.getVolume(), taskEntry.getTempSnapshotId());
            snapshotService.saveSnapshot(taskEntry.getVolume(), taskEntry.getTempSnapshotId());
        } else {
            awsCommunication.deleteSnapshot(taskEntry.getTempSnapshotId());
        }
        taskService.complete(taskEntry);
        LOG.info("{}: Task completed", taskEntry.getId());
        checkThreadInterruption(taskEntry);
        retentionService.apply();
        if(taskEntry.getStatus().equals(COMPLETE_WITH_WARNINGS.getStatus())) {
            notificationService.notifyViaSns(TaskEntry.TaskEntryType.BACKUP, COMPLETE_WITH_WARNINGS, taskEntry.getVolume());
        } else {
            notificationService.notifyViaSns(TaskEntry.TaskEntryType.BACKUP, COMPLETE, taskEntry.getVolume());
        }
        notificationService.notifyAboutSuccess(taskEntry);
    }


    public class AWSBackupVolumeException extends EnhancedSnapshotsException {
        public AWSBackupVolumeException(String message) {
            super(message);
        }
    }

    /**
     * Clean up resources if exception appeared
     *
     * @param tempVolume
     * @param backupFileName
     * @param dto            notification transfer object
     */
    private void deleteTempResources(Volume tempVolume, String backupFileName, TaskProgressDto dto) {
        deleteTempVolume(tempVolume, dto);
        try {
            dto.setMessage("Delete temp file");
            dto.addProgress(10);
            notificationService.notifyAboutTaskProgress(dto);
            storageService.deleteFile(backupFileName);
        } catch (Exception ex) {
            //do nothing if file not found
        }
    }

    private void killInitializationVolumeProcessIfAlive(TaskEntry taskEntry) {
        String fioProcNamePrefix = "fio --filename=" + storageService.detectFsDevName(awsCommunication.getVolume(taskEntry.getTempVolumeId()));
        try {
            // check fio processes are alive
            Process checkFioProcessAlive = new ProcessBuilder("pgrep", "-f", fioProcNamePrefix).start();
            checkFioProcessAlive.waitFor();
            switch (checkFioProcessAlive.exitValue()) {
                case 0:
                    // fio processes are alive
                    LOG.info("{}: Fio processes for volume {} are alive", taskEntry.getId(), taskEntry.getTempVolumeId());
                    Process process = new ProcessBuilder("pkill", "-f", fioProcNamePrefix).start();
                    process.waitFor(3, TimeUnit.MINUTES);
                    switch (process.exitValue()) {
                        case 0:
                            LOG.info("{}: Fio processes for volume {} terminated", taskEntry.getId(), taskEntry.getTempVolumeId());
                            break;
                        default: {
                            LOG.warn("{}: Failed to terminate fio processes for volume {}", taskEntry.getId(), taskEntry.getTempVolumeId());
                        }
                    }
                    break;
                default: {
                    // no need to terminate, fio processes are already terminated
                    LOG.info("{}: Fio processes for volume {} already terminated, no need to stop forcibly", taskEntry.getId(), taskEntry.getTempVolumeId());
                }
            }
        } catch (IOException | InterruptedException e) {
            LOG.warn("{}: Exception while termination of fio processes", taskEntry.getId(), e);
        }
    }

    private String getBackupName(TaskEntry taskEntry) {
        Volume volumeToBackup = awsCommunication.getVolume(taskEntry.getVolume());
        String volumeType = volumeToBackup.getVolumeType();
        String iops = (volumeToBackup.getIops() != null) ? volumeToBackup
                .getIops().toString() : "";
        if (volumeType.equals("")) {
            volumeType = "gp2";
        }
        if (volumeType.equals("standard")) {
            volumeType = "gp2";
        }
        return taskEntry.getVolume() + "." + taskEntry.getStartTime() + "." + volumeType + "." + iops + ".backup";
    }

    private void initializeVolume(TaskEntry taskEntry) {
        String fileNameParam = "--filename=" + storageService.detectFsDevName(awsCommunication.getVolume(taskEntry.getTempVolumeId()));
        ProcessBuilder builder = new ProcessBuilder("fio", fileNameParam, "--rw=randread", "--bs=128k", "--iodepth=32", "--ioengine=libaio", "--direct=1", "--name=volume-initialize");
        try {
            LOG.info("{}: Starting volume {} initialization", taskEntry.getId(), taskEntry.getTempVolumeId());
            Process process = builder.start();
            if (process.isAlive()) {
                LOG.debug("{}: Volume {} initialization started successfully", taskEntry.getId(), taskEntry.getTempVolumeId());
            } else {
                LOG.info("{}: Volume {} initialization failed to start", taskEntry.getId(), taskEntry.getTempVolumeId());
                try (BufferedReader input = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {
                    StringBuilder errorMessage = new StringBuilder();
                    for (String line; (line = input.readLine()) != null; ) {
                        errorMessage.append(line);
                    }
                    LOG.warn(errorMessage);
                }
            }
        } catch (Exception e) {
            LOG.warn("{}: Failed to initialize volume {}", taskEntry.getId(), taskEntry.getTempVolumeId());
            LOG.warn(e);
        }
    }

    private String getFsFreezeLinuxDocument() {
        if (fs_freeze_linux_document_name == null) {
            fs_freeze_linux_document_name = awsCommunication.getDocumentName(FS_FREEZE_LINUX_DOCUMENT_DEFAULT_NAME);
        }
        return fs_freeze_linux_document_name;
    }

    private String getFsUnfreezeLinuxDocument() {
        if (fs_unfreeze_linux_document_name == null) {
            fs_unfreeze_linux_document_name = awsCommunication.getDocumentName(FS_UNFREEZE_LINUX_DOCUMENT_DEFAULT_NAME);
        }
        return fs_unfreeze_linux_document_name;
    }

    private String getCreateVssSnapshotWindowsDocument() {
        if (create_vss_snapshot_windows_document_name == null) {
            create_vss_snapshot_windows_document_name = awsCommunication.getDocumentName(CREATE_VSS_SNAPSHOT_WINDOWS_DOCUMENT_DEFAULT_NAME);
        }
        return create_vss_snapshot_windows_document_name;
    }

    private String getRemoveVssSnapshotWindowsDocument() {
        if (remove_vss_snapshot_windows_document_name == null) {
            remove_vss_snapshot_windows_document_name = awsCommunication.getDocumentName(REMOVE_VSS_SNAPSHOT_WINDOWS_DOCUMENT_DEFAULT_NAME);
        }
        return remove_vss_snapshot_windows_document_name;
    }

    private String getEssHealthCheckDocumentName() {
        if (ess_health_check_document_name == null) {
            ess_health_check_document_name = awsCommunication.getDocumentName(ESS_HEALTH_CHECK_DOCUMENT_DEFAULT_NAME);
        }
        return ess_health_check_document_name;
    }
}
