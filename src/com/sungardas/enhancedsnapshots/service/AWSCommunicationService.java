package com.sungardas.enhancedsnapshots.service;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.services.simplesystemsmanagement.model.CommandInvocation;
import com.amazonaws.services.simplesystemsmanagement.model.DocumentIdentifier;
import com.amazonaws.services.simplesystemsmanagement.model.InstanceInformation;
import com.amazonaws.services.simplesystemsmanagement.model.SendCommandRequest;

import java.util.List;

public interface AWSCommunicationService {

    /**
     * Executes SSM command and waits for the result
     * @param sendCommandRequest
     * @return
     */
    CommandInvocation executeCommandWithSSM(SendCommandRequest sendCommandRequest);

    Snapshot createSnapshot(Volume volume);

    void deleteSnapshot(String snapshotId);

    void cleanupSnapshots(String volumeId, String snapshotIdToLeave);

    Snapshot waitForCompleteState(Snapshot snapshot);

    Snapshot syncSnapshot(String snapshotId);

    Volume waitForVolumeState(Volume volume, VolumeState expectedState);

    Volume syncVolume(Volume volume);

    /**
     * Checks whether volume is root volume
     * @param volumeId
     * @return
     */
    boolean isRootVolume(String volumeId);

    /**
     * Returns device name of volume, null in case volume is not attached
     * @param volumeId
     * @return
     */
    String getDeviceNameOfVolume(String volumeId);

    /**
     * Returns InstanceInformation in case AWS SSM service is supported in region and enabled on instance
     * Throws SdkClientException in case operation is not supported for current region
     * @param instanceId
     * @return
     */
    InstanceInformation getSSMInstanceInformation(String instanceId) throws SdkClientException;

    /**
     * Returns instance id of instance volume belongs to
     * @param volumeId
     * @return instance id of instance volume belongs to or null in case volume is not atttached
     */
    String getInstanceVolumeBelongsTo(String volumeId);

    /**
     * iopsPerGb paramenter is only required for io1 volume type, for other volume types it will be skipped
     */
    Volume createVolumeFromSnapshot(String snapshotId, String availabilityZoneName, VolumeType type, int iopsPerGb);

    void deleteVolume(String volumeId);

    void attachVolume(Instance instance, Volume volume);

	Volume getVolume(String volumeId);

    List<AvailabilityZone> describeAvailabilityZonesForCurrentRegion();

    String getCurrentAvailabilityZone();

    void createTemporaryTag(String resourceId, String description);

    void deleteTemporaryTag(String resourceId);

    Volume createVolume(int size, VolumeType type);

    Volume createIO1Volume(int size, int iopsPerGb);

    Instance getInstance(String instanceId);

    InstanceStatus getInstanceStatus(String instanceId);

    /**
     * Returns list of instances in stopped or running state
     * @return
     */
    List<Instance> getInstances();

    void detachVolume(String volumeId);

    void setResourceName(String resourceid, String value);

    void addTag(String resourceId, String name, String value);

    void addTag(String resourceId, List<Tag> tags);

    boolean snapshotExists(String snapshotId);

    boolean volumeExists(String volumeId);

    Snapshot getSnapshot(String snapshotId);

    void dropS3Bucket(String bucketName);

    //TODO: awslog restarted after each sdfs remount
    //TODO: need to clarify whether we still need restart it
    //TODO: in case we need, it should be triggered by SystemService while restarting sdfs, not by SDFSStateService
    void restartAWSLogService();

    String getDNSName(String instanceId);

    /**
     * Returns SSM document name which is usually stackName-defaultName-id
     * in case stackName is null defaultName will be returned
     * @param defaultName
     * @return
     */
    String getDocumentName(String defaultName);

    /**
     * Returns true in case instance state is not stopped or terminated
     * @param instanceId
     * @return
     */
    boolean instanceIsRunning(String instanceId);
}