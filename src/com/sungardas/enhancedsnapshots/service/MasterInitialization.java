package com.sungardas.enhancedsnapshots.service;

/**
 * Interface for Master node operations.
 * This is means that only for the one copy of service, that implements this interface, will be called init() method
 */
public interface MasterInitialization {
    /**
     * Initialization Master node service method (only one service instance in cluster will be initialized)
     */
    void init();
}
