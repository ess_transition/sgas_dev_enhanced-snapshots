package com.sungardas.enhancedsnapshots.service.impl;

import java.io.StringReader;
import java.util.*;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.simplesystemsmanagement.model.*;
import com.sungardas.enhancedsnapshots.aws.dynamodb.model.BackupEntry;
import com.sungardas.enhancedsnapshots.aws.dynamodb.repository.BackupRepository;
import com.sungardas.enhancedsnapshots.service.AWSCommunicationService;
import com.sungardas.enhancedsnapshots.service.BackupService;

import com.sungardas.enhancedsnapshots.service.SnapshotService;
import com.sungardas.enhancedsnapshots.service.StorageService;
import com.sungardas.enhancedsnapshots.util.OSTypesUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BackupServiceImpl implements BackupService {

    private static final Logger LOG = LogManager.getLogger(BackupServiceImpl.class);

    private static final String BACKUP_FILE_EXT = ".backup";
    private static final String LINUX_PLATFORM = OSTypesUtils.OSTypes.Linux.name();
    private static final String WINDOWS_PLATFORM = OSTypesUtils.OSTypes.Windows.name();
    private static final String[] SUPPORTED_PLATFORMS_FOR_CONSISTENT_BACKUP = {LINUX_PLATFORM, WINDOWS_PLATFORM};
    // for aws ssm documents
    private static final String FS_FREEZE_SUPPORT_DOCUMENT = "ESSLinuxFreezeSupport";
    private static final String FS_FREEZE_SUPPORT_PROPERTY = "FREEZE_SUPPORT";

    @Autowired
    private BackupRepository backupRepository;
    @Autowired
    private StorageService storageService;
    @Autowired
    private SnapshotService snapshotService;
    @Autowired
    private AWSCommunicationService awsCommunicationService;

    private void deleteBackup(BackupEntry backupEntry) {
        try {
            snapshotService.deleteSnapshot(backupEntry.getSnapshotId());
            storageService.deleteFile(backupEntry.getFileName());
            backupRepository.delete(backupEntry);
            LOG.info("Backup {} successfully removed", backupEntry);
        } catch (Exception e) {
            LOG.error("Failed to delete backup {}");
            LOG.error(e);
        }
    }

    @Override
    public List<BackupEntry> getBackupList(String volumeId) {
        return backupRepository.findByVolumeId(volumeId);
    }

    @Override
    public void deleteBackup(String backupName) {
        deleteBackup(backupRepository.findOne(backupName + BACKUP_FILE_EXT));
    }

    @Override
    public void deleteBackup(Collection<BackupEntry> backupEntries) {
        LOG.debug("Removing backups: {}", backupEntries);
        for(BackupEntry backupEntry: backupEntries){
            deleteBackup(backupEntry);
        }
    }

    @Override
    public boolean consistentBackupSupported(String volumeId) {
        String instanceId = awsCommunicationService.getInstanceVolumeBelongsTo(volumeId);
        if (instanceId == null || !awsCommunicationService.instanceIsRunning(instanceId)) {
            LOG.info("Volume {} is not attached or instance is stopped. Backup is consistent by default.", volumeId);
            return true;
        }
        InstanceInformation instanceInformation;
        try {
            instanceInformation = awsCommunicationService.getSSMInstanceInformation(instanceId);
            if (instanceInformation == null) {
                LOG.info("Volume {} is attached to the instance {} which is not accessible for AWS EC2 System Management Service. Consistent backup is not supported.",
                        volumeId, instanceId);
                return false;
            }
            // we will receive SdkClientException in case AWS SSM service is not accessible for current region
        } catch (SdkClientException e) {
            LOG.info("AWS SMS service is not supported in current region. Consistent backup for volume {} is not supported.", volumeId, e);
            return false;
        }
        if (awsCommunicationService.isRootVolume(volumeId)) {
            LOG.info("Consistent backup are not supported for volume {}, since it's a root volume.", volumeId);
            return false;
        }
        if (!platformIsSupportedForConsistentBackup(instanceInformation.getPlatformType())) {
            LOG.info("Consistent backup for volume {} is not supported. Instance {} has unsupported OS type: {}.", volumeId, instanceId, instanceInformation.getPlatformType());
            return false;
        }
        if (OSTypesUtils.isLinuxPlatform(instanceInformation) && !fsIsSupportedForConsistentBackup(volumeId, instanceId)) {
            LOG.info("Consistent backup for volume {} is not supported. Unsupported FS.", volumeId, instanceId);
            return false;
        }
        LOG.info("Consistent backup for volume {} is supported.", volumeId);
        return true;
    }

    @Override
    public Map<String, Boolean> consistentBackupSupported(Map<String, Boolean> result, String ... volumeIds) {
        if (result == null) {
            result = new HashMap<>();
        }
        for (String volumeId : volumeIds) {
            result.put(volumeId, consistentBackupSupported(volumeId));
        }
        return result;
    }

    private boolean platformIsSupportedForConsistentBackup(String platform) {
        return Arrays.asList(SUPPORTED_PLATFORMS_FOR_CONSISTENT_BACKUP).contains(platform);
    }

    private boolean fsIsSupportedForConsistentBackup(String volumeId, String instanceId) {
        try {
            Map<String, List<String>> params = new HashMap<>();
            params.put("device", Arrays.asList(awsCommunicationService.getDeviceNameOfVolume(volumeId)));
            CommandInvocation invocation = awsCommunicationService.executeCommandWithSSM(new SendCommandRequest()
                    .withInstanceIds(instanceId)
                    .withDocumentName(awsCommunicationService.getDocumentName(FS_FREEZE_SUPPORT_DOCUMENT)).withParameters(params));

            Properties properties = new Properties();
            properties.load(new StringReader(invocation.getCommandPlugins().get(0).getOutput()));
            return Boolean.valueOf(properties.getProperty(FS_FREEZE_SUPPORT_PROPERTY));
        } catch (Exception e) {
            LOG.error("Failed to check whether fs freeze operation in supported for volume {}", volumeId);
            LOG.error(e);
            return false;
        }
    }
}
